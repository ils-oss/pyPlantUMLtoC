# pyPlantUMLtoC

PlantUML Class Diagram to C converter, written in Python3

## Syntax

### parse classes
 ```python
 plantuml_to_c.plantuml_to_classes(filename: str) -> CClassList
 ``` 
 parses plantuml file "filename" and returns a list of CClasses.
 Relations and special attributes are already parsed.
 
### create files from class
 ```python
 plantuml_to_c.create_file(target_type: int, directory: str, data: list) -> dict:
 ``` 
 Template folder can be adjusted by setting env to an appropriate jinja2 environment

 Templates need to be defined beforehand:
 ```python
 import plantuml_to_c
 C_FILE = 0
 HDR_FILE = 1
 plantuml_to_c.TEMPLATES[C_FILE] = {"template": 'c_template.c', "dir": "src", "extension": ".c", "override_name": False}
 
 plantuml_to_c.TEMPLATES[HDR_FILE] = {"template": 'header_template.h', "dir": "interface", "extension": ".h",
                                     "override_name": False} 
 ```
 By default, one class will produce one folder and subfolders "src" and "interface"
 
 /src will hold a c-body file
 
 /interface will hold a c-header file
  
 
## Updates

The script will search for files already present and will merge them. Additions will be kept, deletions will be ignored.
