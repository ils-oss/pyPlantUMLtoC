from distutils.core import setup


with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='plantuml_to_c',
    version='0.1',
    packages=['jinja2', 'jinja2-time'],
    url='',
    license='GPLv3',
    author='Bastian Luettig',
    author_email='bastian.luettig@ils.uni-stuttgart.de',
    long_description=long_description,
    long_description_content_type="text/markdown",
    description='A tool to convert plantuml class diagrams into C code, based on templates'
)
