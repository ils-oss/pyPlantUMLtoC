/**
 * \file {{ cls.get_file_name() }}.h
 *
 * \brief Description
 *
 * \version 1.0
 * \author {{ config.AUTHOR_NAME }}
 * \date {% now 'local', "%Y-%m-%d" %}
 *
 * Description
 *
 * @copyright {% now 'local', "%Y" %}
 *
 * Software History:
 *
 * @date {% now 'local', "%Y-%m-%d" %} auto - Generated from plantuml description
 *
 */
#ifndef {{ cls.get_path_name() }}_H
#define {{ cls.get_path_name() }}_H

/* IMPORTED MODULES */
#include "generic.h"
#include "common_data_types.h"
{% for attribute in cls.get_special_attributes() %}
{% if attribute.cclass %}#include "{{ attribute.cclass.get_file_name() }}.h" /* {{ attribute.name }} */{% endif %}{% endfor %}
#include "{{ cls.get_file_name() }}.h"


{% for attribute in cls.constants %}
#define {{attribute.get_name()}} {{attribute.value}}{% endfor %}

/**
 * \brief Access struct
 * Provides access to public attributes and methods of this module
 */
typedef struct S_{{ cls.get_struct_name() }}
{
    /* public class attributes */
    {% for attribute in cls.attributes %}{% if attribute.visibility == "public" %}{{ attribute.get_type_struct() }} {{ attribute.get_member_name() }};
    {% endif %}{% endfor %}
    /* public class methods */
    {% for method in cls.methods %}{% if method.visibility == "public" %}T_FUNC {{ method.get_name() }};
    {% endif %}{% endfor %}
} {{ cls.get_struct_name() }};

{% if cls.class_type == "Singleton" %}
/** \brief get_{{ cls.name }}_instance
 * Get_{{ cls.name }}_Instance returns a singleton instance of {{cls.name}}
 * \return {{cls.get_struct_name()}}*
 *
 */
{{ cls.get_struct_name() }}* Get_{{ cls.name }}_Instance ( void );
{% endif %}

#endif /* {{ cls.get_path_name() }} */
/*EOF*/
