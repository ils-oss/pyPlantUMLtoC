/**
* \file {{ cls.get_file_name() }}.c
*
* \brief {{ cls.name }}
*
* \version 1.0
* \author {{ config.AUTHORNAME }}
* \date {% now 'local', "%Y-%m-%d" %}
*
* Here comes a short description
*
* @copyright {% now 'local', "%Y" %} asd
*
* Software History:
*
* @date {% now 'local', "%Y-%m-%d" %} auto - Generated from plantuml description
*
*/

/* IMPORTED MODULES */
#include "{{ cls.get_file_name() }}.h"
{% for inc in cls.get_include_files() %}
#include "{{ inc }}"{% endfor %}

static {{ cls.get_struct_name() }} instanceData =
{
    /* public class attributes */
    {% for attribute in cls.attributes %}{% if attribute.visibility == "public" %}.{{ attribute.get_member_name() }} = ( {{ attribute.get_type_struct() }} ) 0xFFFFFFFF,
    {% endif %}{% endfor %}
    /* public class methods */
    {% for method in cls.methods %}{% if method.visibility == "public" %}.{{ method.get_name() }} = ( T_FUNC ) 0xFFFFFFFF,
    {% endif %}{% endfor %}
};
static {{ cls.get_struct_name() }}* pInstance = NULL;

/* private attributes */
{% for attribute in cls.attributes %}{% if attribute.visibility == "private" %}{{ attribute.get_type_struct() }} {{ attribute.get_member_name() }}{% if not attribute.array %} = ( {{ attribute.get_type_struct() }} ) 0xFFFFFFFF{% endif %};
{% endif %}{% endfor %}

/* DEFINITION OF LOCAL FUNCTIONS AND PROCEDURES */
/* C module local forward declarations:*/
{% for method in cls.methods %}{% if method.attr == "new" %}
/** \brief {{ method.get_name() }}
*
* {{ method.get_name() }} is called by the user out of a ...
*
* \param parameterIn - (In) ...
* \param parameterOut - (Out) ...
* \return none
* \retval none
*/
static void {{ method.get_name() }} ( void );
{% else %}/* included */ /* {{ method.get_name() }} ( void ); */
{% endif %}{% endfor %}

/* IMPLEMENTATION OF LOCAL FUNCTIONS AND PROCEDURES */
{% for method in cls.methods %}{% if method.attr == "new" %}
static void {{ method.get_name() }} ( void ){
    /* LOCALS */

    /* BODY */
}
{% endif %}{% endfor %}

void Init_Vars()
{
    /* Assign local functions for exposition to local instance */
    {% for method in cls.methods %}{% if method.visibility == "public" %}instanceData.{{ method.get_name() }} = ( T_FUNC ) &{%if method.attr == "new" %}{{ method.get_name() }}{% else %}{{ method.name }}{% endif %};
    {% endif %}{% endfor %}
}

void Set_Vars({% for attribute in cls.attributes %}{% if attribute.cclass.class_type != "Singleton" %}
        {{ attribute.get_type_struct()}} {{ attribute.get_name() }}{% if not loop.last %},{% endif %}{% endif %}{% endfor %})
{
    {% for attribute in cls.get_special_attributes() %}
    /* Assign {{attribute.name}} to local instance */
    {% if attribute.visibility == "public" %}instanceData.{% endif %}{% if attribute.cclass.class_type == "Singleton" %}{{ attribute.get_member_name() }} = ({{ attribute.get_type_struct()}}) {{ attribute.get_instance() }}();{% else %}{{ attribute.get_member_name() }} = {{attribute.get_name()}};{% endif %}
    {% endfor %}
}

{% if cls.class_type == "Singleton" %}
{{ cls.get_struct_name() }}* Get_{{ cls.name }}_Instance ( void )
{
    if(pInstance == NULL) {
        Init_Vars();
        pInstance = &instanceData;
    }
    return pInstance;
}

{% endif %}

/*EOF*/