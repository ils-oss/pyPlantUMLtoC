import re
import pathlib
import os
import difflib
from jinja2 import Environment, PackageLoader, FileSystemLoader

name = "plantuml_to_c"

TEMPLATES = {}
VISIBILITY_PRIVATE = "private"
VISIBILITY_PUBLIC = "public"

ATTRIBUTE_INCLUDED = "included"
ATTRIBUTE_NEW = "new"

TEMPLATE_FOLDER = pathlib.PurePath(os.path.join(os.path.dirname(__file__)), 'templates')

env = ''


def update_loader(folder: str=False):
    global env
    if not folder:
        env = Environment(
            loader=PackageLoader('plantuml_to_c', 'templates')
        )
    else:
        env = Environment(
            loader=FileSystemLoader(folder)
        )
    env.add_extension('jinja2_time.TimeExtension')


update_loader()


class Config:
    AUTHOR_NAME = "dummy"


class RegEx:
    relation = r'(.*)\s+(\".+\"\s)*(\.\.(.*?)>|<(.*?)--)\s+(\".+\"\s)*(.*?)\s*(\:\s*(.*))*\s'
    class_items = r'^\s*(\+|\#)\s*(\+|\#)*\s*([a-zA-z1-9]+)(\(\))*(\s*=\s*(\w*))*(\s*:\s*(.*))*'
    import_items = r'from (.*?)\n\s*\'\s*import:\s*\"(.*?)\"\s*'
    include_items = r'\'\s*include:\s*\"(.*?)\"\s*'
    classes = r'class\s*(.*?)\{(.*?)\}'
    class_type = r"<<\s*(.*?)\s*>>"

    #  Positions inside RegExes
    #  classes
    POS_CLASS_NAME = 1
    POS_CLASS_CONTENT = 2

    # class_items
    POS_VISIBILITY = 1
    POS_VISIBILITY_INCLUDED = 2
    POS_ITEM_NAME = 3
    POS_METHOD_BRACES = 4

    SYMBOL_PUBLIC = "+"
    SYMBOL_PRIVATE = "#"
    SYMBOL_METHOD = "()"

    POS_CONSTANT = 5
    POS_ATTRIBUTE = 7
    POS_ATTRIBUTE_TYPE = 8
    POS_ATTRIBUTE_VALUE = 6

    #  import_items
    POS_IMPORT_NAME = 2

    #  include_items
    POS_INCLUDE_NAME = 1

    #  relations
    POS_RELATION_ARROW = 3
    POS_RELATION_LEFT = 1
    POS_RELATION_RIGHT = 7
    POS_RELATION_TYPE = 9
    ARROW_LEFT = "<"
    RELATION_TYPE_USE = "<<use>>"

    #  diff
    DIFF_FINDER = r"^@@ -(\d+),?(\d+)? \+(\d+),?(\d+)? @@$"
    POS_DIFF_START = 1


class Context(dict):

    def __init__(self, data):
        super().__init__()
        for key, item in data.items():
            self[key] = item
        self['config'] = Config()


class TypedAttribute(object):
    canonical_attributes = ['int64', 'uint64', 'int32', 'uint32', 'uint32*', 'char', 'str']

    def __init__(self, name, atype, visibility):
        self.name = name
        self.atype = re.sub(r"(.*?)\[(.*?)\]", r"\1", atype)
        self.array = not self.atype == atype
        self.visibility = visibility
        self.cclass = False

    def set_class(self, cclass):
        self.cclass = cclass

    # def get_include_file(self):
    #     """
    #     Returns c-header file name
    #     @todo: should only be valid inside classes
    #     :return:
    #     """
    #     return self.atype.replace("[]", "").lower()

    def get_atype(self) -> str:
        return self.atype

    def get_type(self) -> str:
        """
        Returns type, if non canonical attribute -> pointer type
        :return:
        """
        if self.atype in self.canonical_attributes:
            return self.atype
        else:
            return '{0}*'.format(self.atype)

    def get_type_struct(self) -> str:
        """
        returns type for struct.
        @todo: does this make sense? -> move to cclass
        :return:
        """
        if self.atype in self.canonical_attributes:
            return self.atype
        else:
            return 'T_{0}*'.format(self.atype.upper())

    def is_special(self) -> bool:
        """
        returns information if canonical attribute or not
        :return:
        """
        return self.atype not in self.canonical_attributes

    def get_name(self) -> str:
        if self.atype in self.canonical_attributes:
            return self.name
        else:
            return 'p{0}'.format(self.name.title())

    def get_member_name(self) -> str:
        return self.name.lower()

    def get_input_name(self) -> str:
        if self.atype in self.canonical_attributes:
            return self.name
        else:
            return 'In_{0}'.format(self.name)

    def get_struct_name(self) -> str:  # instance
        """
        @todo move to cclass
        :return:
        """
        if self.cclass:
            return self.cclass.get_struct_name()
        else:
            return self.atype

    def get_instance(self) -> str:
        """
        @todo move to cclass
        :return:
        """
        return 'Get_{0}_Instance'.format(self.atype.title())

    def get_path_name(self):
        s1 = re.sub(r'(.)([A-Z][a-z]+)', r'\1_\2', self.atype)
        return re.sub(r'([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


class Constant(TypedAttribute):
    def __init__(self, name, value, atype, visibility):
        super().__init__(name, atype, visibility)
        self.value = value


class Attribute(TypedAttribute):
    def __init__(self, name, visibility):
        super().__init__(name, "generic", visibility)


class Method(object):
    def __init__(self, name, visibility, attr):
        self.name = name
        self.visibility = visibility
        self.attr = attr

    def get_name(self):
        return self.name.title()


class CClass(object):

    def __init__(self, name):
        data = name.split(" ")
        self.name = data[0]
        self.class_type = None
        if data[1]:
            match_str = name.replace(data[0], "")
            match = re.search(RegEx.class_type, match_str, re.MULTILINE)
            if match:
                self.class_type = match.group(1)
        self.attributes = []
        self.methods = []
        self.constants = []
        self.externals = []
        self.includes = []
        self.include_dirs = []
        self.dependencies = []

    def get_path_name(self):
        s1 = re.sub(r'(.)([A-Z][a-z]+)', r'\1_\2', self.name)
        return re.sub(r'([a-z0-9])([A-Z])', r'\1_\2', s1).lower()

    def get_file_name(self):
        return self.name.lower()

    def get_header_file_name(self):
        return '{0}.h'.format(self.get_file_name())

    def add_attribute(self, attribute: str, visibility: str):
        self.attributes.append(Attribute(attribute, visibility))

    def add_typed_attribute(self, attribute: str, atype: str, visibility: str):
        self.attributes.append(TypedAttribute(attribute, atype, visibility))

    def add_method(self, method: str, visibility: str, attr: str):
        self.methods.append(Method(method, visibility, attr))

    def add_external(self, external: str) -> None:
        self.externals.append(external)

    def add_include(self, include: str) -> None:
        inc = pathlib.PurePath(include)
        self.includes.append(inc.name)
        if inc.parent.parent not in self.include_dirs:
            self.include_dirs.append(inc.parent.parent)

    def add_dependency(self, dependency: str) -> None:
        self.dependencies.append(dependency)

    def get_struct_name(self) -> str:
        return 'T_{0}'.format(self.name.upper())

    def get_special_attributes(self):
        return [x for x in self.attributes if x.is_special()]

    def add_constant(self, name, value, atype, visibility):
        cnst = Constant(name, value, atype, visibility)
        self.constants.append(cnst)

    def get_include_files(self) -> list:
        lst = self.includes
        for item in self.dependencies:
            lst.append(item.get_header_file_name())
        return lst

    def get_include_dirs_recursive(self) -> list:
        lst = self.include_dirs
        for item in self.dependencies:
            for i in item.get_include_dirs_recursive():
                if i not in lst:
                    lst.append(i)

        return lst

    def get_dependencies_recursive(self) -> list:
        lst = self.dependencies
        for item in self.dependencies:
            for i in item.get_dependencies_recursive():
                if i not in lst:
                    lst.append(i)
        return lst


class CClassList(list):
    def find(self, name):
        for k, cclass in enumerate(self):
            if cclass.name == name:
                return cclass
        return False

    def update(self, item):
        for k, cclass in enumerate(self):
            if cclass.name == item.name:
                self[k] = item


def match_class_attributes(classes: CClassList) -> CClassList:
    for cls in classes:
        for item in cls.get_special_attributes():
            cclass = classes.find(item.get_atype())
            if cclass:
                cls.add_dependency(cclass)
                item.set_class(cclass)
            else:
                print("not found :(", item.atype)
    return classes


def parse_relations(data: str, classes):
    relations = re.finditer(RegEx.relation, data,
                            re.MULTILINE)

    for rel in relations:
        if rel.group(RegEx.POS_RELATION_ARROW)[0] == RegEx.ARROW_LEFT:
            active = RegEx.POS_RELATION_RIGHT
            passive = RegEx.POS_RELATION_LEFT
        else:
            active = RegEx.POS_RELATION_LEFT
            passive = RegEx.POS_RELATION_RIGHT
        if rel.group(RegEx.POS_RELATION_TYPE) == RegEx.RELATION_TYPE_USE:
            if classes.find(rel.group(active)) and classes.find(rel.group(passive)):
                cls = classes.find(rel.group(active))
                cls.add_dependency(classes.find(rel.group(passive)))
                classes.update(cls)
    return classes


def plantuml_to_classes(filename: pathlib.Path) -> CClassList:
    data = filename.open().read()
    classes = CClassList()
    matches = re.finditer(RegEx.classes, data, re.MULTILINE | re.DOTALL)
    for num, match in enumerate(matches):
        c_class = CClass(match.group(RegEx.POS_CLASS_NAME))

        items = re.finditer(RegEx.class_items, match.group(RegEx.POS_CLASS_CONTENT), re.MULTILINE)
        imports = re.finditer(RegEx.import_items, match.group(RegEx.POS_CLASS_CONTENT), re.MULTILINE)
        includes = re.finditer(RegEx.include_items, match.group(RegEx.POS_CLASS_CONTENT), re.MULTILINE)

        for method in items:

            if method.group(RegEx.POS_VISIBILITY) == RegEx.SYMBOL_PUBLIC:
                visibility = VISIBILITY_PUBLIC
            elif method.group(RegEx.POS_VISIBILITY) == RegEx.SYMBOL_PRIVATE:
                visibility = VISIBILITY_PRIVATE
            else:
                visibility = VISIBILITY_PRIVATE
            if method.group(RegEx.POS_METHOD_BRACES) == RegEx.SYMBOL_METHOD:
                if method.group(RegEx.POS_VISIBILITY_INCLUDED):
                    attr = ATTRIBUTE_INCLUDED
                else:
                    attr = ATTRIBUTE_NEW

                c_class.add_method(method.group(RegEx.POS_ITEM_NAME), visibility, attr)
            elif method.group(RegEx.POS_CONSTANT):
                c_class.add_constant(method.group(RegEx.POS_ITEM_NAME), method.group(RegEx.POS_ATTRIBUTE_VALUE),
                                     method.group(RegEx.POS_ATTRIBUTE_TYPE), visibility)
            elif method.group(RegEx.POS_ATTRIBUTE):
                c_class.add_typed_attribute(method.group(RegEx.POS_ITEM_NAME), method.group(RegEx.POS_ATTRIBUTE_TYPE),
                                            visibility)
            else:
                c_class.add_attribute(method.group(RegEx.POS_ITEM_NAME), visibility)
        for imp in imports:
            c_class.add_external(imp.group(RegEx.POS_IMPORT_NAME))
        for inc in includes:
            c_class.add_include(inc.group(RegEx.POS_INCLUDE_NAME))

        classes.append(c_class)
    classes = match_class_attributes(classes)
    classes = parse_relations(data, classes)
    return classes


def create_file(target_type: int, directory: str, data: list) -> dict:
    tpl = env.get_template(TEMPLATES[target_type]["template"])
    #  generating source file
    if len(data) > 1:
        content = tpl.render(Context({"cls": data}))
        path = pathlib.Path(directory, TEMPLATES[target_type]["dir"])
        path.mkdir(parents=True, exist_ok=True)
    else:
        content = tpl.render(Context({"cls": data[0]}))

        path = pathlib.Path(directory, data[0].get_path_name(), TEMPLATES[target_type]["dir"])
        path.mkdir(parents=True, exist_ok=True)

    if TEMPLATES[target_type]["override_name"]:
        file_path = path.joinpath(TEMPLATES[target_type]["override_name"] + TEMPLATES[target_type]["extension"])
    else:
        file_path = path.joinpath(data[0].get_file_name() + TEMPLATES[target_type]["extension"])

    if file_path.is_file():
        data = list(map(str.rstrip, open(file_path, "r").readlines()))
        content = list(map(str.rstrip, content.splitlines(keepends=True)))
        patch = list(difflib.unified_diff(content, data, lineterm=""))
        content = '\n'.join(content)
        if len(patch) > 1:
            patch_file = '\n'.join(patch)

            content = apply_patch(content, patch_file)

            #  saving original
            f = open(file_path.with_suffix('.orig'), "w")
            f.write('\n'.join(data))
            f.close()

    f = open(file_path, "w")
    f.write(content)
    f.close()

    file_data = {"path": file_path, "content": content}

    return file_data


def apply_patch(original, patch):
    _hdr_pat = re.compile(RegEx.DIFF_FINDER)

    s = original.splitlines(True)
    p = patch.splitlines(True)
    t = ''
    original_cursor = 0
    start_patch = False
    check_plus = False
    buff = ''
    for line in p:
        if line.startswith(("---", "+++",)):
            continue

        match = re.match(_hdr_pat, line)

        if match:
            start_patch = int(match.group(RegEx.POS_DIFF_START)) - 1
            t += ''.join(s[original_cursor:start_patch])
            original_cursor = start_patch
        else:
            #  have we found a match before?
            if not isinstance(start_patch, int):
                print(patch)
                raise Exception("no patch!")

            if check_plus and line[0] != "+":
                t += buff

            check_plus = False
            buff = ''

            if line[0] == " ":
                if line[1:] == s[original_cursor]:
                    t += line[1:]
                original_cursor += 1
            elif line[0] == "+":
                t += line[1:]
            else:  # - detected
                #  we should check next line
                buff = s[original_cursor]
                check_plus = True
                original_cursor += 1
    t += ''.join(s[original_cursor:])
    return t

